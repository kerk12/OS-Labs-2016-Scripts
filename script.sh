#/bin/bash

#Scan all the files located in the working directory.
#Remove all files that have the word "net" on more than 5 lines

fileList=$(ls)

for file in $fileList
do
	if [ -f "$file" ]
	then
		if [ -w "$file" ]
		then
			wordcount=$(grep "net" "$file"|wc -l)
			if [ "$wordcount" -gt 5 ]
			then
				rm "$file"
			fi
		fi
	fi
done
